//
//  StashTests.swift
//  StashTests
//
//  Created by Michael O'Brien on 21/10/16.
//  Copyright © 2016 AreteDigitalDesign. All rights reserved.
//

import XCTest

@testable import Stash

class OtherCustom: NSObject, NSCoding {
  
  var stringValue: String = "string"
  
  override init() { super.init() }
  
  required init?(coder aDecoder: NSCoder) {
    self.stringValue  = aDecoder.decodeObject(forKey: "string") as! String
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(stringValue,  forKey: "string")
  }
}

class CustomObject: NSObject, NSCoding {
  
  var stringValue: String       = "string"
  
  var intValue: Int             = 10
  
  var boolValue: Bool           = false
  
  var arrayValue: [Any]         = ["one", "two", 3, false]
  
  var dictValue: [String : Any] = ["key-one" : "string",
                                   "key-two" : 12,
                                   "key-three" : false,
                                   "key-four" : ["one", "two", 3, false],
                                   "key-five" : ["key" : "pair"]]
  
  var nestedCustom: OtherCustom = OtherCustom()
  
  override init() { super.init() }
  
  required init?(coder aDecoder: NSCoder) {
    self.stringValue  = aDecoder.decodeObject(forKey: "string") as! String
    self.intValue     = aDecoder.decodeInteger(forKey: "int")
    self.boolValue    = aDecoder.decodeBool(forKey: "bool")
    self.arrayValue   = aDecoder.decodeObject(forKey: "array")  as! [Any]
    self.dictValue    = aDecoder.decodeObject(forKey: "dict")   as! [String : Any]
    self.nestedCustom = aDecoder.decodeObject(forKey: "custom") as! OtherCustom
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(stringValue,  forKey: "string")
    aCoder.encode(intValue,     forKey: "int")
    aCoder.encode(boolValue,    forKey: "bool")
    aCoder.encode(arrayValue,   forKey: "array")
    aCoder.encode(dictValue,    forKey: "dict")
    aCoder.encode(nestedCustom, forKey: "custom")
  }
}

class StashTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testReEncrypt() {
    let currentKey : Data      = Crypto.randomEncryptionKey()!
    let newKey     : Data      = Crypto.randomEncryptionKey()!
    let cacheOne   : Stash     = try! Stash(name: "top-level")
    cacheOne.shouldEncryptData = true
    cacheOne.encryptionKey     = currentKey
    
    cacheOne["key-one"] = "string the first"
    cacheOne["key-two"] = 1234
    XCTAssertNotNil(cacheOne["key-one"], "The object should not be nil")
    XCTAssertTrue(cacheOne["key-two"] as? Int == 1234, "The string should not be \"top level string\"")
    cacheOne.purgeAll()
    //Re-Encrypt
    try! cacheOne.reEncrypt(newKey: newKey)
    XCTAssertTrue((cacheOne.encryptionKey?.elementsEqual(newKey))!, "The encryption key should be updated to the new key")
    XCTAssertTrue(cacheOne.object(forKey: "key-one") as? String == "string the first", "The string should not be \"string the first\"")
    XCTAssertTrue(cacheOne.object(forKey: "key-two") as? Int    == 1234, "The int should not be \"1234\"")
  }
  
  func runCustomCompareTests(objOne: CustomObject, objTwo: CustomObject) {
    XCTAssertTrue(objOne.stringValue == objTwo.stringValue, "The returned string value original string value")
    XCTAssertTrue(objOne.intValue    == objTwo.intValue   , "The returned int value original int value")
    XCTAssertTrue(objOne.boolValue   == objTwo.boolValue  , "The returned bool value original bool value")
    //Array
    XCTAssertTrue(objOne.arrayValue[0] as? String == objTwo.arrayValue[0] as? String , "The returned array items original array items")
    XCTAssertTrue(objOne.arrayValue[1] as? String == objTwo.arrayValue[1] as? String , "The returned array items original array items")
    XCTAssertTrue(objOne.arrayValue[2] as? Int    == objTwo.arrayValue[2] as? Int ,    "The returned array items original array items")
    XCTAssertTrue(objOne.arrayValue[3] as? Bool   == objTwo.arrayValue[3] as? Bool ,   "The returned array items original array items")
    //Dict
    XCTAssertTrue(objOne.dictValue["key-one"] as? String == objTwo.dictValue["key-one"] as? String,
                  "The returned dict items original dict items")
    XCTAssertTrue(objOne.dictValue["key-two"] as? Int == objTwo.dictValue["key-two"] as? Int,
                  "The returned dict items original dict items")
    XCTAssertTrue(objOne.dictValue["key-three"] as? Bool == objTwo.dictValue["key-three"] as? Bool,
                  "The returned dict items original dict items")
    //Dict Array
    let objOneArray: [Any] = objOne.dictValue["key-four"] as! [Any];
    let objTwoArray: [Any] = objOne.dictValue["key-four"] as! [Any];
    XCTAssertTrue(objOneArray[0] as? String == objTwoArray[0] as? String , "The returned dict array items original dict array items")
    XCTAssertTrue(objOneArray[1] as? String == objTwoArray[1] as? String , "The returned dict array items original dict array items")
    XCTAssertTrue(objOneArray[2] as? Int    == objTwoArray[2] as? Int ,    "The returned dict array items original dict array items")
    XCTAssertTrue(objOneArray[3] as? Bool   == objTwoArray[3] as? Bool ,   "The returned dict array items original dict array items")
    //Dict Dictionary
    let objOneDict: [String: String] = objOne.dictValue["key-five"] as! [String: String];
    let objTwoDict: [String: String] = objOne.dictValue["key-five"] as! [String: String];
    XCTAssertTrue(objOneDict["key"] == objTwoDict["key"] , "The returned dict items original dict items")
    //Nested
    XCTAssertTrue((objOne.nestedCustom.stringValue == objTwo.nestedCustom.stringValue), "The nested items should match")
  }

  
  
  // MARK: - Subscript Testing
  
  func testSubscriptAltering() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache["sample-string"] = "Sample String"
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    var stringObj = cache["sample-string"]
    XCTAssertNotNil(stringObj, "The object should not be nil")
    XCTAssertTrue((stringObj as! String) == "Sample String", "The object should be 'Sample String' not \(stringObj.debugDescription)")
    //Alter
    cache["sample-string"] = "Sample String Two"
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    stringObj = cache["sample-string"]
    XCTAssertNotNil(stringObj, "The object should not be nil")
    XCTAssertTrue((stringObj as! String) == "Sample String Two", "The object should be 'Sample String Two' not \(stringObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testSubscriptCachingString() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache["sample-string"] = "Sample String"
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let stringObj = cache["sample-string"]
    XCTAssertNotNil(stringObj, "The object should not be nil")
    XCTAssertTrue((stringObj as! String) == "Sample String", "The object should be 'Sample String' not \(stringObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testSubscriptCachingInt() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache["sample-number"] = 123
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let numObj = cache["sample-number"]
    XCTAssertNotNil(numObj, "The object should not be nil")
    XCTAssertTrue((numObj as! Int) == 123, "The object should be '123' not \(numObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testSubscriptCachingArray() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache["sample-array"] = ["one", "two", 3, false]
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let arrayObj: [AnyObject] = cache["sample-array"] as! [AnyObject]
    XCTAssertNotNil(arrayObj, "The object should not be nil")
    XCTAssertTrue((arrayObj[0] as! String) == "one" , "The first object should be \"one\" not \(arrayObj[0])")
    XCTAssertTrue((arrayObj[1] as! String) == "two" , "The first object should be \"two\" not \(arrayObj[1])")
    XCTAssertTrue((arrayObj[2] as! Int)    == 3     , "The first object should be \"3\" not \(arrayObj[2])")
    XCTAssertTrue((arrayObj[3] as! Bool)   == false , "The first object should be \"false\" not \(arrayObj[3])")
    cache.destroyAll()
  }
  
  func testSubscriptCachingDictionary() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache["sample-array"] = [
      "key-one":"string",
      "key-two": 12,
      "key-three" : false,
      "key-four" : ["one", "two", 3, false],
      "key-five" : ["key" : "pair"]
    ]
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let arrayObj: [String : Any] = cache["sample-array"] as! [String : Any]
    XCTAssertNotNil(arrayObj, "The object should not be nil")
    XCTAssertTrue((arrayObj["key-one"] as! String) == "string", "The value for 'key-one' value should be \"string\" not \(arrayObj["key-one"].debugDescription)")
    XCTAssertTrue((arrayObj["key-two"] as! Int) == 12, "The value for 'key-two' value should be '12' not \(arrayObj["key-two"].debugDescription)")
    XCTAssertTrue((arrayObj["key-three"] as! Bool) == false, "The value for 'key-three' value should be 'false' not \(String(describing: arrayObj["key-three"]))")
    let array: [Any]? = arrayObj["key-four"] as? [Any]
    XCTAssertTrue((array?[0] as! String) == "one" , "The first object should be \"one\" not \(String(describing: array?[0]))")
    XCTAssertTrue((array?[1] as! String) == "two" , "The first object should be \"two\" not \(String(describing: array?[1]))")
    XCTAssertTrue((array?[2] as! Int)    == 3     , "The first object should be \"3\" not \(String(describing: array?[2]))")
    XCTAssertTrue((array?[3] as! Bool)   == false , "The first object should be \"false\" not \(String(describing: array?[3]))")
    let dict = arrayObj["key-five"] as! [String : String]
    XCTAssertTrue(dict["key"] == "pair", "The value for 'key-three' value should be 'false' not \(arrayObj["key-three"].debugDescription)")
    cache.destroyAll()
  }
  
  func testSubscriptCachingCustom() {
    let custom = CustomObject()
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache["sample-custom"] = custom
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let customObj = cache["sample-custom"] as! CustomObject
    XCTAssertNotNil(customObj, "The object should not be nil")
    runCustomCompareTests(objOne: custom, objTwo: customObj)
    cache.destroyAll()
  }

  func testSubscriptCachingImage() {
    let imgPath: String = Bundle(for: type(of: self)).path(forResource: "test-image", ofType: "png")!
    let image = UIImage(contentsOfFile: imgPath)
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache["sample-image"] = image
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let imageObj = cache["sample-image"] as! UIImage
    XCTAssertNotNil(imageObj, "The object should not be nil")
    let originalData: Data = UIImagePNGRepresentation(image!)!
    let grabbedData : Data = UIImagePNGRepresentation(imageObj)!
    XCTAssertTrue(originalData.elementsEqual(grabbedData), "The grabbed image data should match the original image data")
    cache.destroyAll()
  }

  
  
  // MARK: - Selector Testing
  
  func testAltering() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject("Sample String", forKey: "sample-string")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    var stringObj = cache.object(forKey: "sample-string")
    XCTAssertNotNil(stringObj, "The object should not be nil")
    XCTAssertTrue((stringObj as! String) == "Sample String", "The object should be 'Sample String' not \(stringObj.debugDescription)")
    //Alter
    cache.setObject("Sample String Two", forKey: "sample-string")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    stringObj = cache.object(forKey: "sample-string")
    XCTAssertNotNil(stringObj, "The object should not be nil")
    XCTAssertTrue((stringObj as! String) == "Sample String Two", "The object should be 'Sample String Two' not \(stringObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testCachingString() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject("Sample String", forKey: "sample-string")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let stringObj = cache.object(forKey: "sample-string")
    XCTAssertNotNil(stringObj, "The object should not be nil")
    XCTAssertTrue((stringObj as! String) == "Sample String", "The object should be 'Sample String' not \(stringObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testCachingInt() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(123, forKey:"sample-number")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let numObj = cache.object(forKey: "sample-number")
    XCTAssertNotNil(numObj, "The object should not be nil")
    XCTAssertTrue((numObj as! Int) == 123, "The object should be '123' not \(numObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testCachingArray() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(["one", "two", 3, false], forKey:"sample-array")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let arrayObj: [AnyObject] = cache.object(forKey: "sample-array") as! [AnyObject]
    XCTAssertNotNil(arrayObj, "The object should not be nil")
    XCTAssertTrue((arrayObj[0] as! String) == "one" , "The first object should be \"one\" not \(arrayObj[0])")
    XCTAssertTrue((arrayObj[1] as! String) == "two" , "The first object should be \"two\" not \(arrayObj[1])")
    XCTAssertTrue((arrayObj[2] as! Int)    == 3     , "The first object should be \"3\" not \(arrayObj[2])")
    XCTAssertTrue((arrayObj[3] as! Bool)   == false , "The first object should be \"false\" not \(arrayObj[3])")
    cache.destroyAll()
  }
  
  func testCachingDictionary() {
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    let cacehArr: [String : Any] = [
      "key-one"   : "string",
      "key-two"   : 12,
      "key-three" : false,
      "key-four"  : ["one", "two", 3, false],
      "key-five"  : ["key" : "pair"]
    ]
    cache.setObject(cacehArr, forKey: "sample-array")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let arrayObj: [String : Any] = cache.object(forKey: "sample-array") as! [String : Any]
    XCTAssertNotNil(arrayObj, "The object should not be nil")
    XCTAssertTrue((arrayObj["key-one"] as! String) == "string", "The value for 'key-one' value should be \"string\" not \(String(describing: arrayObj["key-one"]))")
    XCTAssertTrue((arrayObj["key-two"] as! Int) == 12, "The value for 'key-two' value should be '12' not \(String(describing: arrayObj["key-two"]))")
    XCTAssertTrue((arrayObj["key-three"] as! Bool) == false, "The value for 'key-three' value should be 'false' not \(String(describing: arrayObj["key-three"]))")
    let array: [Any]? = arrayObj["key-four"] as? [Any]
    XCTAssertTrue((array?[0] as! String) == "one" , "The first object should be \"one\" not \(String(describing: array?[0]))")
    XCTAssertTrue((array?[1] as! String) == "two" , "The first object should be \"two\" not \(String(describing: array?[1]))")
    XCTAssertTrue((array?[2] as! Int)    == 3     , "The first object should be \"3\" not \(String(describing: array?[2]))")
    XCTAssertTrue((array?[3] as! Bool)   == false , "The first object should be \"false\" not \(String(describing: array?[3]))")
    let dict = arrayObj["key-five"] as! [String : String]
    XCTAssertTrue(dict["key"] == "pair", "The value for 'key-three' value should be 'false' not \(String(describing: arrayObj["key-three"]))")
    cache.destroyAll()
  }
  
  func testCachingCustom() {
    let custom = CustomObject()
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(custom, forKey: "sample-custom")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let customObj = cache.object(forKey: "sample-custom") as! CustomObject
    XCTAssertNotNil(customObj, "The object should not be nil")
    runCustomCompareTests(objOne: custom, objTwo: customObj)
    cache.destroyAll()
  }
  
  func testCachingImage() {
    let imgPath: String = Bundle(for: type(of: self)).path(forResource: "test-image", ofType: "png")!
    let image = UIImage(contentsOfFile: imgPath)
    let cache = try! Stash(name: "memory cache")
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(image, forKey: "sample-image")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let imageObj = cache.object(forKey: "sample-image") as! UIImage
    XCTAssertNotNil(imageObj, "The object should not be nil")
    let originalData: Data = UIImagePNGRepresentation(image!)!
    let grabbedData : Data = UIImagePNGRepresentation(imageObj)!
    XCTAssertTrue(originalData.elementsEqual(grabbedData), "The grabbed image data should match the original image data")
    cache.destroyAll()
  }

  
  
  // MARK: - Cryptography Testing
  
  func testCryptoCachingString() {
    let cache = try! Stash(name: "memory cache")
    cache.shouldEncryptData = true
    cache.encryptionKey     = Crypto.randomEncryptionKey()
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject("Sample String", forKey: "sample-string")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let stringObj = cache.object(forKey: "sample-string")
    XCTAssertNotNil(stringObj, "The object should not be nil")
    XCTAssertTrue((stringObj as! String) == "Sample String", "The object should be 'Sample String' not \(stringObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testCryptoCachingInt() {
    let cache = try! Stash(name: "memory cache")
    cache.shouldEncryptData = true
    cache.encryptionKey     = Crypto.randomEncryptionKey()
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(123, forKey:"sample-number")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let numObj = cache.object(forKey: "sample-number")
    XCTAssertNotNil(numObj, "The object should not be nil")
    XCTAssertTrue((numObj as! Int) == 123, "The object should be '123' not \(numObj.debugDescription)")
    cache.destroyAll()
  }
  
  func testCryptoCachingArray() {
    let cache = try! Stash(name: "memory cache")
    cache.shouldEncryptData = true
    cache.encryptionKey     = Crypto.randomEncryptionKey()
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(["one", "two", 3, false], forKey:"sample-array")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let arrayObj: [AnyObject] = cache.object(forKey: "sample-array") as! [AnyObject]
    XCTAssertNotNil(arrayObj, "The object should not be nil")
    XCTAssertTrue((arrayObj[0] as! String) == "one" , "The first object should be \"one\" not \(arrayObj[0])")
    XCTAssertTrue((arrayObj[1] as! String) == "two" , "The first object should be \"two\" not \(arrayObj[1])")
    XCTAssertTrue((arrayObj[2] as! Int)    == 3     , "The first object should be \"3\" not \(arrayObj[2])")
    XCTAssertTrue((arrayObj[3] as! Bool)   == false , "The first object should be \"false\" not \(arrayObj[3])")
    cache.destroyAll()
  }
  
  func testCryptoCachingDictionary() {
    let cache = try! Stash(name: "memory cache")
    cache.shouldEncryptData = true
    cache.encryptionKey     = Crypto.randomEncryptionKey()
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    let cacehArr: [String : Any] = [
      "key-one"   : "string",
      "key-two"   : 12,
      "key-three" : false,
      "key-four"  : ["one", "two", 3, false],
      "key-five"  : ["key" : "pair"]
    ]
    cache.setObject(cacehArr, forKey: "sample-array")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let arrayObj: [String : Any] = cache.object(forKey: "sample-array") as! [String : Any]
    XCTAssertNotNil(arrayObj, "The object should not be nil")
    XCTAssertTrue((arrayObj["key-one"] as! String) == "string", "The value for 'key-one' value should be \"string\" not \(arrayObj["key-one"].debugDescription)")
    XCTAssertTrue((arrayObj["key-two"] as! Int) == 12, "The value for 'key-two' value should be '12' not \(arrayObj["key-two"].debugDescription)")
    XCTAssertTrue((arrayObj["key-three"] as! Bool) == false, "The value for 'key-three' value should be 'false' not \(arrayObj["key-three"].debugDescription)")
    let array: [Any]? = arrayObj["key-four"] as? [Any]
    XCTAssertTrue((array?[0] as! String) == "one" , "The first object should be \"one\" not \(String(describing: array?[0]))")
    XCTAssertTrue((array?[1] as! String) == "two" , "The first object should be \"two\" not \(String(describing: array?[1]))")
    XCTAssertTrue((array?[2] as! Int)    == 3     , "The first object should be \"3\" not \(String(describing: array?[2]))")
    XCTAssertTrue((array?[3] as! Bool)   == false , "The first object should be \"false\" not \(String(describing: array?[3]))")
    let dict = arrayObj["key-five"] as! [String : String]
    XCTAssertTrue(dict["key"] == "pair", "The value for 'key-three' value should be 'false' not \(arrayObj["key-three"].debugDescription)")
    cache.destroyAll()
  }
  
  func testCryptoCachingCustom() {
    let custom = CustomObject()
    let cache = try! Stash(name: "memory cache")
    cache.shouldEncryptData = true
    cache.encryptionKey     = Crypto.randomEncryptionKey()
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(custom, forKey: "sample-custom")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let customObj = cache.object(forKey: "sample-custom") as! CustomObject
    XCTAssertNotNil(customObj, "The object should not be nil")
    runCustomCompareTests(objOne: custom, objTwo: customObj)
    cache.destroyAll()
  }

  func testCryptoCachingImage() {
    let imgPath: String = Bundle(for: type(of: self)).path(forResource: "test-image", ofType: "png")!
    let image = UIImage(contentsOfFile: imgPath)
    let cache = try! Stash(name: "memory cache")
    cache.shouldEncryptData = true
    cache.encryptionKey     = Crypto.randomEncryptionKey()
    cache.destroyAll()
    XCTAssertTrue(cache.allObjects().count == 0, "There should be no objects in the cache yet")
    cache.setObject(image, forKey: "sample-image")
    XCTAssertTrue(cache.allObjects().count == 1, "There should be one object in the cache")
    cache.purgeAll()
    //Test that the object is returned
    let imageObj = cache.object(forKey: "sample-image") as! UIImage
    XCTAssertNotNil(imageObj, "The object should not be nil")
    let originalData: Data = UIImagePNGRepresentation(image!)!
    let decrypted   : Data = UIImagePNGRepresentation(imageObj)!
    XCTAssertTrue(originalData.elementsEqual(decrypted), "The decrypted image data should match the original image data")
    cache.destroyAll()
  }

}
