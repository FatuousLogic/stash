//
//  CryptoTests.swift
//  Stash
//
//  Created by Michael O'Brien on 22/10/16.
//  Copyright © 2016 AreteDigitalDesign. All rights reserved.
//

import XCTest

@testable import Stash

class CryptoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testRandomKeys() {
        var usedKeys = [Data]()
        for _ in 0 ..< 100 {
            let random: Data = Crypto.randomEncryptionKey()!
            XCTAssertFalse(usedKeys.contains(random), "The data should not have existed before")
            usedKeys.append(random)
        }
    }
    
    func testStringEncryption() {
        let encryptionKey: Data = Crypto.randomEncryptionKey()!
        let plainString = "This is a plain text that is somewhat readable"
        let plainData   = plainString.data(using: .utf8)
        do {
            if let encryptedData = try Crypto.encryptData(plainString.data(using: .utf8)!, key: encryptionKey) {
                XCTAssertNotNil(encryptedData, "The encrypted data should not be nil")
                XCTAssertNotEqual(encryptedData, plainData, "The encrypted data should not match the plain data")
                let encryptedString = String(data: encryptedData, encoding: .utf8)
                XCTAssertTrue(encryptedString != plainString, "The encrypted string should not match the plain string")
                if let decryptedData = try Crypto.decryptData(encryptedData, key: encryptionKey) {
                    XCTAssertNotNil(decryptedData, "The encrypted data should not be nil")
                    let decryptedString = String(data: decryptedData, encoding: .utf8)
                    XCTAssertTrue(encryptedString != decryptedString, "The encrypted string should not match the decrypted string")
                    XCTAssertTrue(decryptedString == plainString, "The decrypted string should match the plain string")
                }
                else {
                    XCTFail("Error decrypting data")
                }
            }
            else {
                XCTFail("Error encrypting data")
            }
        }
        catch CryptoError.decryptionError(let errorMessage) {
            XCTFail("There should be no decryption errors: \(errorMessage)")
        }
        catch CryptoError.encryptionError(let errorMessage) {
            XCTFail("There should be no encryption errors: \(errorMessage)")
        }
        catch {
            XCTFail("There should be no errors")
        }
    }
    
    func testImageEncryption() {
        let encryptionKey: Data = Crypto.randomEncryptionKey()!
        let plain               = UIImage(named: "test-image.png", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let plainData           = UIImagePNGRepresentation(plain!)
        do {
            if let encryptedData = try Crypto.encryptData(plainData, key: encryptionKey) {
                XCTAssertNotNil(encryptedData, "The encrypted data should not be nil")
                XCTAssertNotEqual(encryptedData, plainData, "The encrypted data should not match the plain data")
                XCTAssertTrue(encryptedData != plainData, "The encrypted data should not match the plain data")
                if let decryptedData = try Crypto.decryptData(encryptedData, key: encryptionKey) {
                    XCTAssertNotNil(decryptedData, "The encrypted data should not be nil")
                    XCTAssertTrue(encryptedData != decryptedData, "The encrypted data should not match the decrypted data")
                    XCTAssertTrue(decryptedData == plainData, "The decrypted data should match the plain data")
                    let image = UIImage(data: decryptedData, scale: 2.0)
                    XCTAssertNotNil(image, "The decrypted image should not be nil")
                    let destURL = URL.init(fileURLWithPath: "/Users/michaelobrien/Desktop/herp.png")
                    try? decryptedData.write(to: destURL, options: .atomic)
                }
                else {
                    XCTFail("Error decrypting data")
                }
            }
            else {
                XCTFail("Error encrypting data")
            }
        }
        catch CryptoError.decryptionError(let errorMessage) {
            XCTFail("There should be no decryption errors: \(errorMessage)")
        }
        catch CryptoError.encryptionError(let errorMessage) {
            XCTFail("There should be no encryption errors: \(errorMessage)")
        }
        catch {
            XCTFail("There should be no errors")
        }
    }
    
    func testArchivedEncryption() {
        let data: [String : Any] = [
            "key-one"   : "string",
            "key-two"   : 23,
            "key-three" : ["array"],
            "key-four"  : ["key":"pair"]
        ]
        let encryptionKey: Data = Crypto.randomEncryptionKey()!
        let plainData           = NSKeyedArchiver.archivedData(withRootObject: data)
        do {
            if let encryptedData = try Crypto.encryptData(plainData, key: encryptionKey) {
                XCTAssertNotNil(encryptedData, "The encrypted data should not be nil")
                XCTAssertNotEqual(encryptedData, plainData, "The encrypted data should not match the plain data")
                XCTAssertTrue(encryptedData != plainData, "The encrypted data should not match the plain data")
                if let decryptedData = try Crypto.decryptData(encryptedData, key: encryptionKey) {
                    XCTAssertNotNil(decryptedData, "The encrypted data should not be nil")
                    XCTAssertTrue(encryptedData != decryptedData, "The encrypted data should not match the decrypted data")
                    XCTAssertTrue(decryptedData == plainData, "The decrypted data should match the plain data")
                    if let decData = NSKeyedUnarchiver.unarchiveObject(with: decryptedData) as? [String : Any] {
                        XCTAssertTrue((decData["key-one"] as! String) == "string", "The key-one value should be 'string'")
                        XCTAssertTrue((decData["key-two"] as! Int) == 23, "The key-two value should be '23'")
                        XCTAssertTrue((decData["key-three"] as! [String]) == ["array"], "The key-three value should be '[\"array\"]'")
                        XCTAssertTrue((decData["key-four"] as! [String : String]) == ["key" : "pair"], "The key-three value should be '[\"key\" : \"pair\"]'")
                    }
                    else {
                        XCTFail("The data should have unarchived")
                    }
                    
                }
                else {
                    XCTFail("Error decrypting data")
                }
            }
            else {
                XCTFail("Error encrypting data")
            }
        }
        catch CryptoError.decryptionError(let errorMessage) {
            XCTFail("There should be no decryption errors: \(errorMessage)")
        }
        catch CryptoError.encryptionError(let errorMessage) {
            XCTFail("There should be no encryption errors: \(errorMessage)")
        }
        catch {
            XCTFail("There should be no errors")
        }
    }

    func testPerformanceExample() {
        let encryptionKey: Data = Crypto.randomEncryptionKey()!
        let plain               = UIImage(named: "test-image.png", in: Bundle(for: type(of: self)), compatibleWith: nil)
        let plainData           = UIImagePNGRepresentation(plain!)
        self.measure {
            do {
                let encryptedData = try Crypto.encryptData(plainData, key: encryptionKey)
                try Crypto.decryptData(encryptedData, key: encryptionKey)
            }
            catch {
                XCTFail("There should be no errors")
            }
        }
    }
    
}
