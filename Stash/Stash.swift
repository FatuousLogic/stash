//
//  Stash.swift
//  Stash
//
//  Created by Michael O'Brien on 22/10/16.
//  Copyright © 2016 AreteDigitalDesign. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation

public enum CacheExpiry {
  case never
  case seconds(TimeInterval)
  case date(Foundation.Date)
}

public enum StashError : Error {
  case encryptionError(String)
  case valueError(String)
}

/**
 A wrapper around NSCache that also writes to disk.
 */
public class Stash: NSObject {

  /// The file extension to use when writing to disk
  private static let fileExt: String = "stashdata"

  /// The cache name
  public let name: String

  /// The cache directory
  public let cacheDirectory: URL

  /// The internal memory cache
  internal let cache: NSCache<NSString, StashObject> = NSCache()

  /// Bool flag whether to encrypt data before it is written to disk
  public var shouldEncryptData: Bool = false

  /// Bool flag whether or not to read from the disk asynchrounously. Default is synchrounously.
  public var shouldReadAsync: Bool = false

  /// The key to use when encrypting data. This is required if encryptData is set to true
  public var encryptionKey: Data? = nil

  /// The internal file manager
  fileprivate let fileManager = FileManager()

  /// The queue to fire off any data tasks for
  fileprivate let queue = DispatchQueue(label: "com.stash.diskQueue",
                                        qos: .background,
                                        attributes: DispatchQueue.Attributes.concurrent,
                                        autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit,
                                        target: nil)
  /**
   Will return a stash cache instance with the given name

   - Parameter name:           The cache name
   - Parameter cacheDirectory: Optional cache directory to use. Defualt will be the root cache dir
   with the name appended as path component
   - Parameter fileProtection: Optional file protection type string. Needs to be NSFileProtectionKey

   - Returns: Stash instance
   */
  required public init(_ name: String, cacheDirectory cacheDir: URL?) throws {
    self.name  = name
    cache.name = name
    if let cacheDir = cacheDir {
      cacheDirectory = cacheDir
    }
    else {
      let cacheDir: URL   = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
      self.cacheDirectory = cacheDir.appendingPathComponent("com.stash.cache/" + name)
    }
    //Create dir if needed
    try fileManager.createDirectory(at: cacheDirectory, withIntermediateDirectories: true, attributes: nil)
  }

  /**
   Will return a cache instance with the given name

   - Parameter name: The cache name
   */
  public convenience init(name: String) throws {
    try self.init(name, cacheDirectory: nil)
  }




  //MARK: Subscript
  open subscript(key: String) -> Any? {
    get {
      return object(forKey: key)
    }
    set(newValue) {
      //Can't call generic type method from subscript so manually doing things here
      if let value = newValue {
        setObject(value, forKey: key)
      } else {
        destroyObject(forKey: key)
      }
    }
  }

  /**
   Will return a random encryption key using the given length

   - Parameter length: The length for the key
   */
  public class func randomEncryptionKey() -> Data? {
    return Crypto.randomDataOfLength(length: 64)
  }



  //MARK: Private

  /**
   Will return a cleaned version of the given key

   - Parameter dirtyKey: The original key

   - Returns: String or nil
   */
  fileprivate func cleanKey(_ dirtyKey: String) -> String? {
    return dirtyKey.replacingOccurrences(of: "[^a-zA-Z0-9_]+", with: "-", options: .regularExpression, range: nil)
  }

  /**
   Will return the file path URL to the cached data with the given key

   - Parameter key: The key

   - Returns: URL or nil
   */
  fileprivate func filePath(forKey key: String) -> URL? {
    if let cleanKey = self.cleanKey(key) {
      return cacheDirectory.appendingPathComponent(cleanKey).appendingPathExtension(Stash.fileExt)
    }
    return nil
  }

  /**
   Will return a StashObject instance for the given key

   - Parameter key: The key the object was saved under

   - Returns: StashObject instance or nil
   */
  fileprivate func read(_ key: String) -> StashObject? {
    guard let object = cache.object(forKey: key as NSString) else {
      if let fileURL = filePath(forKey: key) {
        if fileManager.fileExists(atPath: fileURL.path) {
          return unarchive(fileURL: fileURL)
        }
      }
      return nil
    }
    return object
  }

  /**
   Will write the given object to disk as a StashObject class

   - Parameter key: The key the object was saved under

   - Returns: StashObject instance or nil
   */
  fileprivate func write(_ object: StashObject, key: String) throws {
    cache.setObject(object, forKey: key as NSString)
    do {
      if let fileURL = filePath(forKey: key) {
        if shouldEncryptData {
          guard let _ = encryptionKey else { throw StashError.encryptionError("Encryption is enabled but there is no valid encryption key set") }
          let plainData = NSKeyedArchiver.archivedData(withRootObject: object)
          let encrypted = try Crypto.encryptData(plainData, key: encryptionKey)
          try encrypted?.write(to: fileURL, options: .atomic)
        }
        else {
          NSKeyedArchiver.archiveRootObject(object, toFile: fileURL.path)
        }
      }
    }
  }

  /**
   Will return a stash cache instance with the given name

   - Parameter fileURL: The URL to the file to unarchive

   - Returns: StashObject instance
   */
  fileprivate func unarchive(fileURL: URL) -> StashObject? {
    do {
      var archived = try Data.init(contentsOf: fileURL)
      if shouldEncryptData {
        guard let encryptionKey = encryptionKey else { throw StashError.encryptionError("Encryption is enabled but there is no valid encryption key set") }
        archived = try Crypto.decryptData(archived, key: encryptionKey)!
      }
      return NSKeyedUnarchiver.unarchiveObject(with: archived) as? StashObject
    }
    catch CryptoError.decryptionError(let error) {
      print("Error decypting cached data at file path: \(fileURL.path) with Error: \(error)")
      return nil
    }
    catch {
      print("Error unarchiving cached data at file path \(fileURL.path)")
      return nil
    }
  }

  /**
   Will remove the data for the given key from the disk

   - Parameter key: The key for the data to remove
   */
  fileprivate func removeFromDisk(_ key: String) {
    if let fileURL = filePath(forKey: key) {
      _ = try? fileManager.removeItem(at: fileURL)
    }
  }

  /**
   Will remove the data for the given key from the in-memory cache

   - Parameter key: The key for the object to remove
   */
  fileprivate func removeFromMemory(_ key: String) {
    cache.removeObject(forKey: key as NSString)
  }

  /**
   Will return all cache keys currently managed by the cache.

   - Parameter excludes: An array of items to exclude

   - Returns: Array of key strings
   */
  public func allKeys(excludes: [String] = [String]()) -> [String] {
    if let urls = try? fileManager.contentsOfDirectory(at: self.cacheDirectory, includingPropertiesForKeys: nil, options: []) {
      let mapped = urls.flatMap { $0.deletingPathExtension().lastPathComponent }
      return (excludes.count == 0) ? mapped : mapped.filter({ (string) -> Bool in return (!excludes.contains(string)) })
    }
    return []
  }



  //MARK: Object Getting/Setting/Checking

  /**
   Will remove any expired objects from disk and from memory
   */
  public func removeExpired() {
    queue.sync(flags: .barrier, execute: {
      let keys: [String] = self.allKeys(excludes: [".DS_Store"])
      for key in keys {
        let potential = self.read(key)
        if let potential = potential, potential.isExpired {
          self.cache.removeObject(forKey: key as NSString)
          self.removeFromDisk(key)
        }
      }
    })
  }

  /**
   Will set the given object for the given key. If the object is nil - the cached and memory data
   will be removed.

   - Parameter object:     The object to stash
   - Parameter forKey:     The key to stash the object under
   - Parameter expiration: The expiration type
   */
  public func setObject(_ object: Any?, forKey key: String, expiration: CacheExpiry = .never) {
    if object == nil {
      cache.removeObject(forKey: key as NSString)
      removeFromDisk(key)
    }
    else {
      let stashObj = StashObject(value: object as! NSCoding, expiration: expiration)
      queue.sync(flags: .barrier, execute: {
        do {
          try self.write(stashObj, key: key)
        }
        catch {
          print("Error stashing obejct")
        }
      })
    }
  }

  /**
   Will return the currently stashed object for the given key

   - Parameter forKey:        The key for the stashed data
   - Parameter returnExpired: Bool whether to return expired objects

   - Returns: The cashed object or nil
   */
  public func object(forKey key: String, returnExpired: Bool = false) -> Any? {
    var object: StashObject?
    queue.sync {
      object = self.read(key)
    }
    if let object = object, !object.isExpired || returnExpired {
      return object.value
    }
    return nil
  }

  /**
   Will remove all objects from the in-memory cache
   */
  public func purgeAll() {
    cache.removeAllObjects()
  }

  /**
   Will remove the object for the given key from the in-memory cache

   - Parameter key: The cache key for the data to remove
   */
  public func purgeObject(forKey key: String) {
    cache.removeObject(forKey: key as NSString)
  }

  /**
   Will remove the object for the given key from the in-memory and on-disk cache

   - Parameter key: The cache key for the data to remove
   */
  public func destroyObject(forKey key: String) {
    purgeObject(forKey: key)
    queue.sync(flags: .barrier, execute: {
      self.removeFromDisk(key)
    })
  }

  /**
   Will remove all objects from the in-memory and on-disk cache
   */
  public func destroyAll() {
    purgeAll()
    queue.sync(flags: .barrier, execute: {
      let keys: [String] = self.allKeys(excludes: [".DS_Store"])
      for key in keys {
        self.removeFromDisk(key)
      }
    })
  }

  /**
   Will return all objects currently managed by the stash.

   - Parameter excludes: An array o data keys to exclude

   - Returns: Array of objects
   */
  public func allObjects(excludes: [String] = [String]()) -> [Any] {
    var fExcludes = [String]()
    fExcludes.append(contentsOf: excludes)
    let keys         : [String]      = allKeys(excludes: fExcludes)
    let stashObjects : [StashObject] = keys.flatMap({ self.read($0) })
    return stashObjects.flatMap({ $0.value })
  }

  /**
   Will load all objects from disk into memory.

   - Parameter excludes: An array o data keys to exclude
   */
  public func loadAllObjects(excludes: [String] = [String]()) {
    queue.sync(flags: .barrier, execute: {
      let _ = self.allObjects()
    })
  }

  /**
   Will re-encrypt all current objects by using the current key to decrypt them and then
   encrypting them with the given new key

   - Parameter newKey: The new encryption key to use

   - Throws: CryptoError when encryption/decryption errors occur
   */
  public func reEncrypt(newKey: Data?) throws {
    if !shouldEncryptData { throw CryptoError.invalidDataError("Encryption is not enabled or current key is invalid") }
    guard let _  = encryptionKey else { throw CryptoError.invalidDataError("Current encryption key is invalid") }
    guard let nKey = newKey      else { throw CryptoError.invalidDataError("New key is invalid or nil") }
    try! queue.sync(flags: .barrier, execute: {
      let keys: [String] = allKeys(excludes: [".DS_Store"])
      for key in keys {
        if let stashObject = self.read(key) {
          if let fileURL  = filePath(forKey: key) {
            let plainData = NSKeyedArchiver.archivedData(withRootObject: (stashObject as AnyObject))
            let encrypted = try Crypto.encryptData(plainData, key: nKey)
            do {
              try encrypted?.write(to: fileURL)
            }
            catch {
              print("Error stashing obejct")
            }
          }
        }
      }
      self.encryptionKey = nKey
    })
  }

}
