//
//  StashObject.swift
//  Stash
//
//  Created by Michael O'Brien on 22/10/16.
//  Copyright © 2016 AreteDigitalDesign. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation

internal enum StashObjectError : Error {
  case invalidValue(String)
}

internal class StashObject: NSObject, NSCoding {

  /// The cache object
  private(set) var value: NSCoding

  /// The expiration date for the data written to disk
  private(set) var diskExpiry: Date

  /**
   Will return an expiration date for the given enum

   - Parameter expiry: The expiry for the object
   */
  private class func expirationDateForExpiry(_ expiry: CacheExpiry = .never) -> Date {
    switch expiry {
    case .never:
      return Date.distantFuture
    case .seconds(let seconds):
      return Date().addingTimeInterval(seconds)
    case .date(let date):
      return date
    }
  }

  /// Bool flag indicating if the object's disk limit has expired or not
  var isExpired : Bool {
    return (diskExpiry.timeIntervalSinceNow < 0)
  }

  /// Bool flag indicating if the object's disk limit has expired or not
  init (value aValue: NSCoding, expiration: CacheExpiry = .never) {
    self.value      = aValue
    self.diskExpiry = StashObject.expirationDateForExpiry(expiration)
  }

  required init?(coder aDecoder: NSCoder) {
    guard let aValue  = aDecoder.decodeObject(forKey: "value") else {
      return nil
    }
    guard let diskExp = aDecoder.decodeObject(forKey: "diskExpiry") as? Date else {
      return nil
    }

    self.value      = aValue as! NSCoding
    self.diskExpiry = diskExp
  }

  func encode(with aCoder: NSCoder) {
    aCoder.encode(value,      forKey: "value")
    aCoder.encode(diskExpiry, forKey: "diskExpiry")
  }

}
