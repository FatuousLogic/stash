//
//  Crypto.swift
//  Stash
//
//  Created by Michael O'Brien on 30/8/16.
//  Copyright © 2016 Michael O'Brien. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation

public enum CryptoError : Error {
    case invalidDataError(String)
    case encryptionError(String)
    case decryptionError(String)
}

private extension String {
    var hexData: Data? {
        var data = Data(capacity: characters.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, characters.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            var num = UInt8(byteString, radix: 16)!
            data.append(&num, count: 1)
        }
        guard data.count > 0 else { return nil }
        return data
    }
}

internal extension Data {
    var hexString: String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}

private extension String {
    var UTF8CString: UnsafeMutablePointer<Int8> {
        return UnsafeMutablePointer(mutating: (self as NSString).utf8String!)
    }
}

class Crypto: NSObject {

    /// The error domain for any cryptographic errors
    private static let errorDomain: String = "com.arete.CryptKeeper"

    /// The algorithm to use
    private static let algorithm: CCAlgorithm = CCAlgorithm(kCCAlgorithmAES128)

    /// The algorithm key size
    private static let keySize: Int    = kCCKeySizeAES256

    /// The algorithm block size
    private static let blockSize: Int  = kCCBlockSizeAES128

    /// The algorithm IV size
    private static let IVSize: Int     = kCCBlockSizeAES128

    /// The algorithm salt size
    private static let saltSize: Int   = 32

    /// The number of PBKD rounds to run
    private static let PBKDRounds: Int = 10000

    /// The IVRange to use when encrypting/decrypting
    private static let IVRange: NSRange = NSRange(location: 0, length: kCCBlockSizeAES128)

    /// The range of the salte
    private class func saltRangeForData(_ data: Data) -> NSRange {
        return NSRange(location: (data.count - Crypto.saltSize), length: Crypto.saltSize)
    }

    /**
     Will return a random encryption key using the given length
     
     - Parameter length: The length for the key
     */
    final class func randomEncryptionKey(length: Int = 64) -> Data? {
        return randomDataOfLength(length: length)
    }

    /**
     Will return a random encryption key using the given length
     
     - Parameter length: The length for the key
     */
    internal class func randomDataOfLength(length: Int = 0) -> Data? {
        let bytes = [UInt32](repeating: 0, count: length).map { _ in arc4random() }
        return Data(bytes: bytes, count: length)
    }

    private class func encryptionKeyForKey(_ key: Data, salt: Data) -> Data {
        let encStr : String = key.hexString
        let saltStr: String = salt.hexString
        var dataKey: Data   = Data(count: Crypto.keySize)
        let _ = dataKey.withUnsafeMutableBytes { (mutableBytes) in
            CCKeyDerivationPBKDF(CCPBKDFAlgorithm(kCCPBKDF2),
                                 encStr.UTF8CString,
                                 encStr.utf8.count,
                                 [UInt8](saltStr.utf8),
                                 saltStr.utf8.count,
                                 CCPseudoRandomAlgorithm(kCCPRFHmacAlgSHA1),
                                 UInt32(Crypto.PBKDRounds),
                                 mutableBytes,
                                 Crypto.keySize)
        }
        return dataKey
    }

    private class func dataRangeValid(range: NSRange, data: Data?) -> Bool {
        guard let fData = data else { return false }
        if range.location > fData.count { return false }
        if (range.location + range.length) > fData.count { return false }
        return (range.location != NSNotFound && range.location >= 0)
    }

    private class func encrypted(data: Data, key: Data, iv: Data?, salt: Data?) throws -> Data? {
        guard let iv   = iv   else { throw CryptoError.encryptionError("The given iv is invalid or nil") }
        guard let salt = salt else { throw CryptoError.encryptionError("The given salt is invalid or nil") }
        let encKey     = encryptionKeyForKey(key, salt: salt)
        
        var cipherData = Data(count: (data.count + blockSize))
        var outLength: size_t = 0

        let status = cipherData.withUnsafeMutableBytes { (mutableBytes) -> CCCryptorStatus in
            return CCCrypt(CCOperation(kCCEncrypt),
                           Crypto.algorithm,
                           CCOptions(kCCOptionPKCS7Padding),
                           [UInt8](encKey),
                           encKey.count,
                           [UInt8](iv),
                           [UInt8](data),
                           data.count,
                           mutableBytes,
                           cipherData.count,
                           &outLength)
        }
        if status == CCCryptorStatus(kCCSuccess) {
            var finalData = Data()
            finalData.append(iv)
            finalData.append(cipherData)
            finalData.append(salt)
            return finalData
        }
        else {
            throw CryptoError.encryptionError("Encryption failed with error \(status)")
        }
    }

    private class func decrypted(data: Data, key: Data, iv: Data?, salt: Data?) throws -> Data? {
        if !dataRangeValid(range: IVRange, data: data) { return nil }
        if !dataRangeValid(range: saltRangeForData(data), data: data) { return nil }
        let fRange: NSRange = NSMakeRange(IVSize, data.count - (IVSize + saltSize))
        if !dataRangeValid(range: fRange, data: data) { return nil }

        let IVData: Data = iv ?? data.subdata(in: NSMakeRange(0, IVSize).toRange()!)

        var saltData: Data? = salt
        if saltData == nil {
            saltData = data.subdata(in: NSMakeRange(data.count - saltSize, saltSize).toRange()!)
        }
        guard let _ = saltData else { return nil }
        
        var atData: Data = iv ?? data.subdata(in: NSMakeRange(IVSize, data.count - (saltSize + IVSize)).toRange()!)
        
        let decKey: Data = encryptionKeyForKey(key, salt: saltData!)
        
        var outLength: size_t = 0
        
        var decrypted: Data = atData
        let status = decrypted.withUnsafeMutableBytes { (mutableBytes) -> CCCryptorStatus in
            return CCCrypt(CCOperation(kCCDecrypt),
                           algorithm,
                           CCOptions(kCCOptionPKCS7Padding),
                           [UInt8](decKey),
                           decKey.count,
                           [UInt8](IVData),
                           [UInt8](atData),
                           atData.count,
                           mutableBytes,
                           decrypted.count,
                           &outLength)
        }
        if status == CCCryptorStatus(kCCSuccess) {
            let start = decrypted.startIndex
            let end   = min(decrypted.count, (decrypted.endIndex - blockSize))
            return decrypted.subdata(in: start..<end)
        }
        else {
            throw CryptoError.encryptionError("Decryption failed with error \(status)")
        }
    }

    /**
     Will encrypt the given data using the user's stored crypto password
 
     - Parameter data: The data to encrypt
 
     - Throws: `CryptoError.encryptionError`
     */
    @discardableResult
    internal class func encryptData(_ data: Data?, key: Data?) throws -> Data? {
        guard let data = data else { throw CryptoError.invalidDataError("The given data is nil") }
        guard let key  = key  else { throw CryptoError.invalidDataError("The given encryption key is nil") }
        var encrypted: Data?
        do {
            encrypted = try Crypto.encrypted(data: data,
                                              key: key,
                                              iv: randomDataOfLength(length: Crypto.IVSize),
                                              salt: randomDataOfLength(length: Crypto.saltSize))
        }
        catch CryptoError.invalidDataError(let errorMessage) {
            throw CryptoError.encryptionError(errorMessage)
        }
        catch {
            throw CryptoError.encryptionError("Unknown error occured")
        }
      return encrypted
    }

    /**
     Will decrypt the given data using the user's stored crypto password
     
     - Parameter data: The data to decrypt
     
     - Throws: `CryptoError.decryptionError`
     */
    @discardableResult
    internal class func decryptData(_ data: Data?, key: Data) throws -> Data? {
      guard let data = data else { throw CryptoError.decryptionError("Invalid or nil data") }
        var decrypted: Data?
        do {
            decrypted = try Crypto.decrypted(data: data, key: key, iv: nil, salt: nil)
        }
        catch CryptoError.invalidDataError(let errorMessage) {
            throw CryptoError.decryptionError(errorMessage)
        }
        catch {
            throw CryptoError.decryptionError("Unknown error occured")
        }
        return decrypted
    }

}
