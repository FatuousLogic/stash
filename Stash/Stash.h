//
//  Stash.h
//  Stash
//
//  Created by Michael O'Brien on 21/10/16.
//  Copyright © 2016 AreteDigitalDesign. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Stash.
FOUNDATION_EXPORT double StashVersionNumber;

//! Project version string for Stash.
FOUNDATION_EXPORT const unsigned char StashVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Stash/PublicHeader.h>


